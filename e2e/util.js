import { Selector, ClientFunction } from "testcafe";

export const testURL = process.env.URL_SITE;

export const testUser = {
  instructor: {
    email: process.env.INSTRUCTOR_EMAIL,
    password: process.env.INSTRUCTOR_PASSWORD
  },
  admin: {
    email: process.env.ADMIN_EMAIL,
    password: process.env.ADMIN_PASSWORD
  },
  student: {
    email: process.env.STUDENT_EMAIL,
    password: process.env.STUDENT_PASSWORD
  }
};

export const scrollUp = ClientFunction(y => {
  window.scrollBy(0, y);
});

export const scrollDown = ClientFunction(y => {
  window.scrollBy(0, y);
});

export function testSelector(name) {
  return Selector(`[data-test='${name}']`);
}

export function signInWithTestUser({ user, t }) {
  return t
    .typeText(Selector('input[name="email"]'), user.email)
    .typeText(Selector('input[name="password"]'), user.password)
    .click(
      Selector('button[type="button"] > span > span > p').withText("Sign In")
    )
    .wait(5000)
    .expect(ClientFunction(() => document.location.pathname)())
    .eql("/feed");
}

export const testKeys = {
  enter: "enter",
  deleteAllText: "ctrl+a delete",
  backspace: "backspace"
};

export const elementByXPath = Selector(xpath => {
  const iterator = document.evaluate(
    xpath,
    document,
    null,
    XPathResult.UNORDERED_NODE_ITERATOR_TYPE,
    null
  );
  const items = [];

  let item = iterator.iterateNext();

  while (item) {
    items.push(item);
    item = iterator.iterateNext();
  }

  return items;
});
