import { Selector, t, ClientFunction } from "testcafe";
import {
  testURL,
  signInWithTestUser,
  testUser,
  testKeys,
  scrollUp,
  testSelector
} from "./util";
import testData from "./testData";

fixture.only`courses`.page`${testURL}`.beforeEach(async t => {
  await signInWithTestUser({ user: testUser.instructor, t });
  const mobile = await testSelector("bottom-navigation").exists;
  // await t.click(Selector('button[aria-label="menu"]'));
  if (!mobile) {
    await t.click(testSelector("show-menu"));
  }
  await t.click(testSelector("my-courses"));
});

test("should create instructor paced public course", async t => {
  const programming = testData.courses[0];
  await t
    //     // .click(Selector('span.MuiListItemText-primary').withExactText('Courses'))
    .click(testSelector("create-course"))
    .click(testSelector("instructor-paced"))
    .click(testSelector("technology"))
    .click(testSelector("public"))
    .typeText(testSelector("course-title"), programming.title)
    .typeText(testSelector("course-subtitle"), programming.subTitle)
    .typeText(testSelector("course-code"), programming.code)
    .typeText(testSelector("course-description"), programming.description);
  for (let element of programming.tags) {
    await t
      .typeText(testSelector("course-tags"), element)
      .pressKey(testKeys.enter);
  }
  await t
    .click(testSelector("next-button"))
    .click(testSelector("create-button"))
    .expect(testSelector("snack-bar").textContent)
    .eql("Course Created");
});

test("should create instructor paced private course", async t => {
  const discreteMathematics = testData.courses[1];
  await t
    //     // .click(Selector('span.MuiListItemText-primary').withExactText('Courses'))
    .click(testSelector("create-course"))
    .click(testSelector("instructor-paced"))
    .click(testSelector("technology"))
    .click(testSelector("private"))
    .typeText(testSelector("course-title"), discreteMathematics.title)
    .typeText(testSelector("course-subtitle"), discreteMathematics.subTitle)
    .typeText(testSelector("course-code"), discreteMathematics.code)
    .typeText(
      testSelector("course-description"),
      discreteMathematics.description
    );
  for (let element of discreteMathematics.tags) {
    await t
      .typeText(testSelector("course-tags"), element)
      .pressKey(testKeys.enter);
  }
  await t
    .click(testSelector("next-button"))
    .click(testSelector("create-button"))
    .expect(testSelector("snack-bar").textContent)
    .eql("Course Created");
});

test("should create instructor paced organization course", async t => {
  const physics = testData.courses[2];
  await t
    //     // .click(Selector('span.MuiListItemText-primary').withExactText('Courses'))
    .click(testSelector("create-course"))
    .click(testSelector("instructor-paced"))
    .click(testSelector("technology"))
    .click(testSelector("organization"))
    .click(testSelector("organization-name"))
    .click(testSelector("phinma-ui"))
    .typeText(testSelector("course-title"), physics.title)
    .typeText(testSelector("course-subtitle"), physics.subTitle)
    .typeText(testSelector("course-code"), physics.code)
    .typeText(testSelector("course-description"), physics.description);
  for (let element of physics.tags) {
    await t
      .typeText(testSelector("course-tags"), element)
      .pressKey(testKeys.enter);
  }
  await t
    .click(testSelector("next-button"))
    .click(testSelector("create-button"))
    .expect(testSelector("snack-bar").textContent)
    .eql("Course Created");
});

test("should create self paced public course", async t => {
  const english = testData.courses[3];
  await t
    //     // .click(Selector('span.MuiListItemText-primary').withExactText('Courses'))
    .click(testSelector("create-course"))
    .click(testSelector("self-paced"));
  await scrollUp(-500);
  await t
    .click(testSelector("teaching-&-academics"))
    .click(testSelector("public"))
    .typeText(testSelector("course-title"), english.title)
    .typeText(testSelector("course-subtitle"), english.subTitle)
    .typeText(testSelector("course-code"), english.code)
    .typeText(testSelector("course-description"), english.description);
  for (let element of english.tags) {
    await t
      .typeText(testSelector("course-tags"), element)
      .pressKey(testKeys.enter);
  }
  await t
    .click(testSelector("next-button"))
    .click(testSelector("create-button"))
    .expect(testSelector("snack-bar").textContent)
    .eql("Course Created");
});

test("should create self paced private course", async t => {
  const filipino = testData.courses[4];
  await t
    //     // .click(Selector('span.MuiListItemText-primary').withExactText('Courses'))
    .click(testSelector("create-course"))
    .click(testSelector("self-paced"));
  await scrollUp(-500);
  await t
    .click(testSelector("teaching-&-academics"))
    .click(testSelector("private"))
    .typeText(testSelector("course-title"), filipino.title)
    .typeText(testSelector("course-subtitle"), filipino.subTitle)
    .typeText(testSelector("course-code"), filipino.code)
    .typeText(testSelector("course-description"), filipino.description);
  for (let element of filipino.tags) {
    await t
      .typeText(testSelector("course-tags"), element)
      .pressKey(testKeys.enter);
  }
  await t
    .click(testSelector("next-button"))
    .click(testSelector("create-button"))
    .expect(testSelector("snack-bar").textContent)
    .eql("Course Created");
});

test("should create self paced organization course", async t => {
  const dbms = testData.courses[5];
  await t
    //     // .click(Selector('span.MuiListItemText-primary').withExactText('Courses'))
    .click(testSelector("create-course"))
    .click(testSelector("self-paced"));
  await scrollUp(-500);
  await t
    .click(testSelector("teaching-&-academics"))
    .click(testSelector("organization"))
    .click(testSelector("organization-name"))
    .click(testSelector("cpu"))
    .typeText(testSelector("course-title"), dbms.title)
    .typeText(testSelector("course-subtitle"), dbms.subTitle)
    .typeText(testSelector("course-code"), dbms.code)
    .typeText(testSelector("course-description"), dbms.description);
  for (let element of dbms.tags) {
    await t
      .typeText(testSelector("course-tags"), element)
      .pressKey(testKeys.enter);
  }
  await t
    .click(testSelector("next-button"))
    .click(testSelector("create-button"))
    .expect(testSelector("snack-bar").textContent)
    .eql("Course Created");
});
