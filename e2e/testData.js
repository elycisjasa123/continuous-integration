const courses = [
  {
    title: "Programming",
    subTitle: "Coding is Fun",
    code: "PROG 001",
    description:
      "Programming is the process of taking an algorithm and encoding it into a notation, a programming language, so that it can be executed by a computer. Although many programming languages and many different types of computers exist, the important first step is the need to have the solution.",
    tags: [
      "simple expression",
      "functions",
      "logical expression",
      "mathematical expression",
      "loops"
    ],
    topics: [
      {
        title: "What is programming?",
        content:
          'Programming is the process of creating a set of instructions that tell a computer how to perform a task. Programming can be done using a variety of computer "languages," such as SQL, Java, Python, and C++.',
        tags: ["program", "instruction", "logic"],
        comments: []
      }
    ],
    modules: [],
    quizzes: [],
    classes: []
  },
  {
    title: "Discrete Mathematics",
    subTitle: "Math is fun",
    code: "MAT 001",
    description:
      "Mathematics includes the study of such topics as quantity, structure, space, and change. It has no generally accepted definition. Mathematicians seek and use patterns to formulate new conjectures; they resolve the truth or falsity of conjectures by mathematical proof.",
    tags: ["addition", "subtraction", "division", "multiplication", "numbers"],
    topics: [
      {
        title: "What is cuberoot?",
        content:
          "In mathematics, a cube root of a number x is a number y such that y³ = x. All nonzero real numbers, have exactly one real cube root and a pair of complex conjugate cube roots, and all nonzero complex numbers have three distinct complex cube roots.",
        tags: ["cuberoot", "numbers", "counting"],
        comments: []
      }
    ],
    modules: [
      {
        name: "Module 1",
        description:
          'Discrete mathematics is the study of mathematical structures that are fundamentally discrete rather than continuous. In contrast to real numbers that have the property of varying "smoothly", the objects studied in discrete mathematics – such as integers, graphs, and statements in logic'
      }
    ],
    quizzes: [
      {
        name: "Quiz 1",
        description: "This is a quiz description 1",
        question: []
      },
      {
        name: "Quiz 2",
        description: "This is a quiz description 2",
        question: [
          {
            type: "Multiple Choice",
            description: "One plus one equals ?",
            tags: []
          }
        ]
      }
    ],
    classes: [
      {
        name: "Class 2",
        code: "CLAS 002",
        startTime: "1030AM",
        endTime: "1200PM"
      }
    ]
  },
  {
    title: "Physics",
    subTitle: "Physics is fun",
    code: "PHY 001",
    description:
      "Physics is the natural science that studies matter, its motion and behavior through space and time, and the related entities of energy and force. Physics is one of the most fundamental scientific disciplines, and its main goal is to understand how the universe behaves.",
    tags: ["motion", "science", "matter", "space and time", "energy", "force"],
    topics: [
      {
        title: "What is space time?",
        content:
          "In physics, spacetime is any mathematical model which fuses the three dimensions of space and the one dimension of time into a single four-dimensional manifold. Spacetime diagrams can be used to visualize relativistic effects, such as why different observers perceive where and when events occur differently.",
        tags: ["space", "time", "dimensions"],
        comments: []
      }
    ],
    modules: [],
    quizzes: [],
    classes: []
  },
  {
    title: "English",
    subTitle: "English is fun",
    code: "ENG 001",
    description:
      "English is a West Germanic language that was first spoken in early medieval England and eventually became a global lingua franca. It is named after the Angles, one of the Germanic tribes that migrated to the area of Great Britain that later took their name, as England.",
    tags: ["language", "grammar", "spoken", "pronounciation"],
    topics: [
      {
        title: "What is Antonyms?",
        content: "a word opposite in meaning to another ",
        tags: ["opposite", "meaning", "word"],
        comments: []
      }
    ],
    modules: [],
    quizzes: [],
    classes: []
  },
  {
    title: "Filipino",
    subTitle: "Filipino ay masaya",
    code: "FIL 001",
    description:
      "Kahit saan mang sulok ng Pilipinas ay may kanya-kanyang wika na ginagamit ang bawat mamamayang Pilipino. Iba't-ibang wikang ginagamit sa pakikipag kumunikasyon, pakikipag ugnayan at pagpapahayag. Ang iba'y mahirap bigkasin, mahirap intindihin ngunit iisa lamang ang layunin, ang ipaabot sa bawat isa ang sinisigaw ng damdamin.",
    tags: ["wika", "pagbasa", "pananalita", "pagbigkas"],
    topics: [
      {
        title: "Ano ang kahulugan ng Matulungin?",
        content: "Ugali na laging tumutulong na bukal sa kalooban",
        tags: ["ugali", "kapwa", "tulong"],
        comments: []
      }
    ],
    modules: [],
    quizzes: [],
    classes: []
  },
  {
    title: "Database Management System",
    subTitle: "Learn to manage all your stored data",
    code: "DBMS 001",
    description:
      "A database management system (DBMS) is a software package designed to define, manipulate, retrieve and manage data in a database. A DBMS generally manipulates the data itself, the data format, field names, record structure and file structure. It also defines rules to validate and manipulate this data.",
    tags: ["data", "information", "sql", "query"],
    topics: [
      {
        title: "What is the different of data and information?",
        content:
          "Data is raw, unorganized facts that need to be processed. Data can be something simple and seemingly random and useless until it is organized. While the information is that When data is processed, organized, structured or presented in a given context so as to make it useful, it is called information.",
        tags: ["data", "information", "management"],
        comments: [
          {
            content: "This is my first comment",
            reply: ["This is my reply on first comment 1"]
          },
          {
            content: "This is my second comment",
            reply: ["This is my reply on second comment 1"]
          }
        ]
      }
    ],
    modules: [],
    quizzes: [],
    classes: []
  }
];

const dbmsCourseComments = courses[5].topics[0].comments;
const selectedCourse = courses[1];
const selectedQuiz = selectedCourse.quizzes[0];

const testData = {
  courses,
  dbmsCourseComments,
  selectedCourse,
  selectedQuiz
};

export default testData;
